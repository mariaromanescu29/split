from django.urls import path

from split import views

urlpatterns = [
    path('', views.HomepageView.as_view(), name='home'),
    path('create_product/', views.ProductCreateView.as_view(), name='create-product'),
    path('list_product/', views.ProductListView.as_view(), name='list-product'),
    path('update_product/<int:pk>', views.ProductUpdateView.as_view(), name='update-product'),
    path('delete_product/<int:pk>', views.product_delete_view, name='delete-product'),
    path('detail_product/<int:pk>', views.ProductDetailView.as_view(), name='detail-product'),
    path('create_category/', views.CategoryCreateView.as_view(), name='create-category'),
    path('detail_category/<int:pk>', views.CategoryDetailView.as_view(), name='detail-category'),
    path('list_category/', views.CategoryListView.as_view(), name='list-category'),
]
