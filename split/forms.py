from django import forms
from django.forms import TextInput, Select, ModelForm
from split.models import Product, Category


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = ['category', 'name', 'description', 'size', 'colors',
                  'lender', 'image']  # campurile pe care le vrem in formular
        widgets = {
            'category': Select(attrs={'data-msg': 'Introduceti categoria produsului', 'class': 'form-control'}),
            'name': TextInput(attrs={'data-msg': 'Introduceti numele produsului', 'class': 'form-control'}),
            'description': forms.Textarea(
                attrs={'data-msg': 'Introduceti descrierea produsului', 'class': 'form-control'}),
            'size': TextInput(attrs={'class': 'form-control'}),
            'colors': TextInput(attrs={'data-msg': 'Introduceti culoarea produsului', 'class': 'form-control'}),
            'lender': Select(attrs={'class': 'form-control'})
        }


def __init__(self, *args, **kwargs):
    super(ProductForm, self).__init__(*args, **kwargs)
    self.fields['name'].label = 'Denumirea produsului'
    self.fields['category'].required = False


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = ['name', 'picture']
        widgets = {
            'name': Select(attrs={'class': 'form-control'}),
        }
