from rest_framework import serializers

from split.models import Product


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product  # tabela unde sunt stocate datele
        fields = ['name', 'category', 'description', 'size', 'colors', 'image', 'lender']

# ajuta la trimiterea datelor specificate de developer
