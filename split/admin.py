from django.contrib import admin
from split.models import Category, Product

admin.site.register(Category)
admin.site.register(Product)
