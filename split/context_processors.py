from split.models import Category


def get_common_data(request):
    return {'categories': Category.objects.all()}
