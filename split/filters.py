import django_filters

from split.models import Product, Category


class ProductFilters(django_filters.FilterSet):
    class Meta:
        model = Product
        fields = ['category', 'name', 'description', 'size', 'colors', 'lender']


class CategoryFilters(django_filters.FilterSet):
    class Meta:
        model = Category
        fields = ['name']
