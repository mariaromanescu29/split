from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.
from customers.models import Customer


class Category(models.Model):
    name = models.CharField(_('Categorie'), max_length=50)
    picture = models.ImageField(null=True, blank=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.name}'


class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(_('Titlu anunt'), max_length=50)
    description = models.CharField(_('Descriere'), max_length=350, null=True, blank=True)
    size = models.CharField(_('Marime'), max_length=350, null=True, blank=True)
    colors = models.CharField(_('Culoare'), max_length=350, null=True, blank=True)
    image = models.ImageField(upload_to='static/')
    available = models.BooleanField(default=True)
    lender = models.ForeignKey(Customer, on_delete=models.CASCADE)
    deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(_('Created At'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Updated_At'), auto_now_add=True)

    def __str__(self):
        return f'{self.name}'
