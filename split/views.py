from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render, redirect

from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, ListView, UpdateView, DeleteView, DetailView
from rest_framework import viewsets

from split.forms import ProductForm, CategoryForm
from split.models import Product, Category
from split.serializers import ProductSerializer
from split.filters import ProductFilters, CategoryFilters


class HomepageView(TemplateView):
    template_name = 'homepage.html'


class ProductCreateView(LoginRequiredMixin, CreateView):
    template_name = 'split/create_product.html'
    model = Product
    success_url = reverse_lazy('create_advert')
    form_class = ProductForm
    # permission_required = 'split.add_product'


class ProductListView(ListView):
    template_name = 'split/list_product.html'
    model = Product
    context_object_name = 'all_products'
    queryset = Product.objects.filter(deleted=False)

    # permission_required = 'split.view_list_products'

    def get_context_data(self, **kwargs):
        data = super(ProductListView, self).get_context_data(**kwargs)
        all_products = self.queryset

        my_filter = ProductFilters(self.request.GET, queryset=all_products)
        all_products = my_filter.qs
        data['all_products'] = all_products
        data['my_filter'] = my_filter
        return data


class ProductUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'split/update_product.html'
    model = Product
    success_url = reverse_lazy('list-product')
    form_class = ProductForm
    # permission_required = 'split.change_product'


# class ProductDeleteView(LoginRequiredMixin, DeleteView):
#     template_name = 'split/delete_product.html'
#     model = Product
#     success_url = reverse_lazy('list-product')

def product_delete_view(request, pk):
    product = Product.objects.get(pk=pk)
    if request.method == 'POST':
        product.deleted = True
        product.save()
        return redirect(reverse_lazy("list-product"))
    return render(request, 'split/delete_product.html', {'product': product})


class ProductDetailView(LoginRequiredMixin, DetailView):
    template_name = 'split/detail_product.html'
    model = Product


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


### Category ###

class CategoryCreateView(LoginRequiredMixin, CreateView):
    template_name = 'split/create_category.html'
    model = Category
    success_url = reverse_lazy('create_category')
    form_class = CategoryForm
    # permission_required = 'customer.add_category'


class CategoryListView(LoginRequiredMixin, ListView):
    template_name = 'split/list_category.html'
    model = Category
    context_object_name = 'all_category'
    queryset = Category.objects.filter(deleted=False)

    def get_context_data(self, **kwargs):
        data = super(CategoryListView, self).get_context_data(**kwargs)
        all_category = self.queryset

        my_filter = CategoryFilters(self.request.GET, queryset=all_category)
        all_category = my_filter.qs
        data['all_category'] = all_category
        data['my_filter'] = my_filter
        return data


class CategoryDetailView(LoginRequiredMixin, DetailView):
    template_name = 'split/detail_category.html'
    model = Category
    context_object_name = "category"

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data()
        category = context[self.context_object_name]
        context['products'] = Product.objects.filter(category=category)
        return context
