from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.
class Address(models.Model):
    address = models.CharField(_('Adresa'), max_length=100)
    country = models.CharField(_('Tara'), max_length=30)
    city = models.CharField(_('Judet'), max_length=30)
    locality = models.CharField(_('Localitate'), max_length=30)
    postal_code = models.IntegerField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)  # accepta null si bydefaul ia valoarea isi ia valoarea blank
    latitude = models.FloatField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.locality} | {self.city}'


class Customer(models.Model):
    location = models.ForeignKey(Address, on_delete=models.CASCADE)
    first_name = models.CharField(_('Prenume'), max_length=50)
    last_name = models.CharField(_('Nume'), max_length=50)
    phone = models.CharField(max_length=30)
    email = models.EmailField()
    deleted = models.BooleanField(default=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)  # one customer has one user
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Point(models.Model):
    location = models.ForeignKey(Address, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    number = models.IntegerField()
    datetime = models.DateTimeField(auto_now_add=True)
    deleted = models.BooleanField(default=False)
