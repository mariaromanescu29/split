from django.urls import path

from customers import views
from admin_panel import views as admin_views

urlpatterns = [path('create_customers', admin_views.register_user, name='create-customers'),
               path('create_address/', views.AddressCreateView.as_view(), name='create-address'),
               path('list_address/', views.AddressListView.as_view(), name='list-address'),
               path('update_address/<int:pk>', views.AddressUpdateView.as_view(), name='update-address'),
               path('delete_address/<int:pk>', views.address_delete_view, name='delete-address'),
               path('detail_address/<int:pk>', views.AddressDetailView.as_view(), name='detail-address'),
               path('create_point/', views.PointCreateView.as_view(), name='create-point'),
               path('list_point/', views.PointCreateView.as_view(), name='list-point'),
               path('detail_point/<int:pk>', views.PointDetailView.as_view(), name='detail_point'),
               ]
