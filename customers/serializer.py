from rest_framework import serializers

from customers.models import Address


class AddressSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Address  # tabela unde sunt stocate datele
        fields = ['address', 'country', 'city', 'locality', 'postal_code']
