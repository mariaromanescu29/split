from customers.models import Address, Point

import django_filters


class AddressFilters(django_filters.FilterSet):
    class Meta:
        model = Address
        fields = ['address', 'country', 'city', 'locality', 'postal_code']


class PointFilters(django_filters.FilterSet):
    class Meta:
        model = Point
        fields = ['customer', 'location', 'number', 'datetime']
