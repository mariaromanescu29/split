from django import forms

from customers.models import Address, Customer

from django.forms import TextInput, ModelForm, Select


class AddressForm(ModelForm):
    class Meta:
        model = Address
        fields = ['address', 'country', 'city', 'locality', 'postal_code']
        widgets = {
            'address': TextInput(attrs={'data-msg': 'Introduceti adresa', 'class': 'form-control'}),
            'country': TextInput(attrs={'data-msg': 'Introduceti tara', 'class': 'form-control'}),
            'city': TextInput(attrs={'data-msg': 'Introduceti orasul', 'class': 'form-control'}),
            'locality': TextInput(attrs={'data-msg': 'Introduceti localitatea', 'class': 'form-control'}),
            'postal code': TextInput(attrs={'data-msg': 'Introduceti codul postal', 'class': 'form-control'}),
        }


class CustomerFrom(ModelForm):
    class Meta:
        model = Customer
        fields = '__all__'
        widgets = {'location': forms.Select(attrs={'data-msg': 'Locatie'}),
                   'first_name': TextInput(attrs={'data-msg': 'Introduceti prenumele', 'class': 'form-control'}),
                   'last_name': TextInput(attrs={'data-msg': 'Introduceti numele', 'class': 'form-control'}),
                   'phone': TextInput(attrs={'data-msg': 'Introduceti nr. de telefon', 'class': 'form-control'}),
                   'email': TextInput(attrs={'data-msg': 'Introduceti adresa de email', 'class': 'form-control'}),
                   }
        exclude = ['user', 'deleted']
