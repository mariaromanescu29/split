from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.shortcuts import render, redirect

from customers.models import Address, Point
from customers.serializer import AddressSerializer
from django.urls import reverse_lazy
from rest_framework import viewsets
from django.views.generic import CreateView, UpdateView, DeleteView, ListView, DetailView

from customers.forms import AddressForm
from customers.filters import AddressFilters, PointFilters


class AddressCreateView(LoginRequiredMixin, CreateView):
    template_name = 'customers/create_address.html'
    model = Address
    success_url = reverse_lazy('create_address')
    form_class = AddressForm
    # permission_required = 'customer.add_address'


class AddressListView(LoginRequiredMixin, ListView):
    template_name = 'customers/list_address.html'
    model = Address
    context_object_name = 'all_address'
    queryset = Address.objects.filter(deleted=False)

    # permission_required = 'customers.view_list_address'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        all_address = self.queryset

        my_filter = AddressFilters(self.request.GET, queryset=all_address)
        all_address = my_filter.qs
        data['all_address'] = all_address
        data['my_filter'] = my_filter
        return data


class AddressUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'customers/update_address.html'
    model = Address
    success_url = reverse_lazy('list-address')
    form_class = AddressForm
    # permission_required = 'customers.change_address'


# class AddressDeleteView(LoginRequiredMixin, DeleteView):
#     template_name = 'customers/delete_address.html'
#     model = Address
#     success_url = reverse_lazy('list-address')


def address_delete_view(request, pk):
    address = Address.objects.get(pk=pk)
    if request.method == 'POST':
        address.deleted = True
        address.save()
        return redirect(reverse_lazy("list-address"))
    return render(request, 'customers/delete_address.html', {'address': address})


class AddressDetailView(LoginRequiredMixin, DetailView):
    template_name = 'customers/detail_address.html'
    model = Address


class AddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer


### Point ###

class PointCreateView(LoginRequiredMixin, CreateView):
    template_name = 'customers/create_point.html'
    model = Point
    success_url = reverse_lazy('create_point')
    form_class = AddressForm
    # permission_required = 'customer.add_point'


class PointListView(LoginRequiredMixin, ListView):
    template_name = 'split/list_point.html'
    model = Point
    context_object_name = 'all_point'
    queryset = Point.objects.filter(deleted=False)

    def get_context_data(self, **kwargs):
        data = super(PointListView, self).get_context_data(**kwargs)
        all_point = self.queryset

        my_filter = PointFilters(self.request.GET, queryset=all_point)
        all_point = my_filter.qs
        data['all_point'] = all_point
        data['my_filter'] = my_filter
        return data


class PointDetailView(LoginRequiredMixin, DeleteView):
    template_name = 'customers/detail_point.html'
    model = Point
