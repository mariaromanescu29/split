# Generated by Django 4.0 on 2022-01-15 19:36

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('customers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductReservation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_datetime', models.DateField(default=django.utils.timezone.now)),
                ('end_datetime', models.DateField(default=django.utils.timezone.now)),
                ('status', models.BooleanField()),
                ('deleted', models.BooleanField(default=False)),
                ('borrower', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='customers.customer')),
            ],
        ),
    ]
