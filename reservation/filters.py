import django_filters

from reservation.models import ProductReservation


class ReservationFilters(django_filters.FilterSet):
    class Meta:
        model = ProductReservation
        fields = ['borrower', 'start_datetime', 'end_datetime', 'status']
