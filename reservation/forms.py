from django import forms
from django.forms import TextInput, ModelForm, Select
from reservation.models import ProductReservation


class ReservationForm(ModelForm):
    class Meta:
        model = ProductReservation
        fields = ['borrower', 'start_datetime', 'end_datetime']
        widgets = {
            'borrower': forms.Select(
                attrs={'data-msg': 'Introduceti persoana careia imprumutati', 'class': 'form-control'}),
            'start_datetime': forms.DateInput(format='%d-%m-%y',
                                              attrs={'data-msg': 'Introduceti data de inceput', 'type': 'date',
                                                     'class': 'form-control'}),
            'end_datetime': forms.DateInput(format='%d-%m-%y',
                                            attrs={'data-msg': 'Introduceti data de sfarsit', 'type': 'date',
                                                   'class': 'form-control'})
        }
