from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render, redirect

from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from rest_framework.viewsets import ModelViewSet

from reservation.forms import ReservationForm
from reservation.models import ProductReservation
from reservation.serializers import ReservationSerializer
from reservation.filters import ReservationFilters


class ReservationCreateView(LoginRequiredMixin, CreateView):
    template_name = 'reservation/create_reservation.html'
    model = ProductReservation
    success_url = reverse_lazy('create_reservation')
    form_class = ReservationForm

    # permission_required = 'split.add_reservation'


class ReservationListView(LoginRequiredMixin, ListView):
    template_name = 'reservation/list_reservation.html'
    model = ProductReservation
    context_object_name = 'all_reservation'
    queryset = ProductReservation.objects.filter(deleted=False)

    # permission_required = 'split.view_list_reservation'

    def get_context_data(self, **kwargs):
        data_r = super().get_context_data(**kwargs)
        all_reservation = self.queryset

        my_filter_reservation = ReservationFilters(self.request.GET, queryset=all_reservation)
        all_reservation = my_filter_reservation.qs
        data_r['all_reservation'] = all_reservation
        data_r['my_filter_reservation'] = my_filter_reservation
        return data_r


class ReservationUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'reservation/update_reservation.html'
    model = ProductReservation
    success_url = reverse_lazy('list-reservation')
    form_class = ReservationForm
    # permission_required = 'split.change_product'


# class ReservationsDeleteView(LoginRequiredMixin, DeleteView):
#     template_name = 'reservation/delete_reservation.html'
#     model = ProductReservation
#     success_url = reverse_lazy('list-reservation')

def reservation_delete_view(request, pk):
    reservation = ProductReservation.objects.get(pk=pk)
    if request.method == 'POST':
        reservation.deleted = True
        reservation.save()
        return redirect(reverse_lazy("list-reservation"))
    return render(request, 'reservation/delete_reservation.html', {'reservation': reservation})


class ReservationDetailView(LoginRequiredMixin, DetailView):
    template_name = 'reservation/detail_reservation.html'
    model = ProductReservation


class ProductViewSet(ModelViewSet):
    queryset = ProductReservation.objects.all()
    serializer_class = ReservationSerializer
