from django.db import models
from django.utils import timezone

from customers.models import Customer


class ProductReservation(models.Model):
    borrower = models.ForeignKey(Customer, on_delete=models.CASCADE)
    start_datetime = models.DateField(default=timezone.now)
    end_datetime = models.DateField(default=timezone.now)
    status = models.BooleanField()
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.start_datetime} {self.end_datetime}'
