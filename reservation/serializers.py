from rest_framework import serializers

from reservation.models import ProductReservation


class ReservationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProductReservation
        fields = ['borrower', 'start_datetime', 'end_datetime', 'status']
