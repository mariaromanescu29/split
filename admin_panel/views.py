from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login
from django.core.mail import send_mail
from django.shortcuts import render, redirect

from admin_panel.forms import UserFrom
from customers.forms import CustomerFrom


def register_user(request):
    if request.method == 'GET':
        user_form = UserFrom()
        profile_form = CustomerFrom()
    else:
        user_form = UserFrom(request.POST)
        profile_form = CustomerFrom(request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            profile = profile_form.save()
            profile.user = user
            profile.save()

            subject = 'Welcome to Split'
            message = 'We are glad you care about others and chose to be part of our community!'
            send_mail(
                subject,
                message,
                settings.EMAIL_HOST_USER,
                [user.email]
            )

            login(request, user)
            return redirect('homepage')
        else:
            messages.error(request, 'An error has occured during registration')

    context = {'user_form': user_form, 'profile_form': profile_form}
    return render(request, 'users/create_customer.html', context)
